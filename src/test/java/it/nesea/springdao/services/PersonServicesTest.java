package it.nesea.springdao.services;

import it.nesea.springdao.service.PersonServices;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
public class PersonServicesTest {
    @Autowired
    private PersonServices personServices;

    @Test
    public void testRepoSave(){
        long size = personServices.countByRepo();
        personServices.saveStubPersonRepo();
        Assert.assertTrue(size < personServices.countByRepo());
    }
}
