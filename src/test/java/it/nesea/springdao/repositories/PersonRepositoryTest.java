package it.nesea.springdao.repositories;

import it.nesea.springdao.model.Person;
import it.nesea.springdao.repository.PersonRepository;
import it.nesea.springdao.repository.projections.NoID;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
public class PersonRepositoryTest {
    @Autowired
    PersonRepository personRepository;

    @Test
    public void testFindByEmailLike(){
        List<Person> personList = personRepository.findByEmailLike("nesea.it");
        Assert.assertTrue(personList != null && !personList.isEmpty());
    }

    @Test
    public void testMyCustomSearchByName(){
        List<Person> persons = personRepository.myCustomSearchByName("Michele");
        Assert.assertTrue(persons != null && !persons.isEmpty());
    }

    @Test
    public void testFindByLastName(){
        Optional<Person> person = personRepository.findById(1);
        Assert.assertTrue(person.isPresent());
    }

    @Test
    public void testFindByName(){
        Person person = personRepository.findByName("Flavio");
        System.out.println("person = " + person);
        Assert.assertTrue(person != null && person.getName().equalsIgnoreCase("Flavio"));
    }

    @Test
    public void testFindByEmailAndName(){
        List<NoID> personList = personRepository.findByEmailAndLastName("luigi.bianchi@nesea.it", "Bianchi");
        Assert.assertTrue(personList != null && !personList.isEmpty());
    }

}
