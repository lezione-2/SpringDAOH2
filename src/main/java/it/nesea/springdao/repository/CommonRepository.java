package it.nesea.springdao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;
import java.util.Optional;

/**
 * Questa interfaccia viene marcata come @NoRepositoryBean poiché serve ad indicare a Spring che non deve generare
 * alcun codice java per lei. L'implementazione verrà demandata per le interfacce marcate come @Repository o terminali
 * (Aka senza l'annotation NoRepositoryBean)
 *
 * Lo scopo di questo genere di approccio sta nel poter generalizzare quanto possibile i repository
 * evitando che vi siano dei metodi comuni o replicati, ad esempio il findById.
 * Questo non fa altro che aggiungere un ulteriore velo alla stratificazione che infine, per lo sviluppatore finale
 * è del tutto trasparente.
 * @param <T>
 * @param <I>
 */

@NoRepositoryBean
public interface CommonRepository <T, I extends Serializable> extends JpaRepository<T, I> {
    Optional<T> findById(I id);
    <S extends T> S save (S entity);
}
