package it.nesea.springdao.repository.specifications;

import it.nesea.springdao.model.Person;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class PersonSpecification implements Specification<Person> {
    private transient SearchCriteria criteria;

    public PersonSpecification(SearchCriteria criteria) {
        this.criteria = criteria;
    }

    /**
     * Anzichè sviluppare dei metodi custom all'interno del DAO, cosa che ci può spingere sia
     * ad astrarre ulteriormente le interfaccie, sia anche a raggionare nell'ottica
     * di database. Orientandosi alle collection possiamo avvalerci delle specifications.
     *
     * Questo fa si che possiamo continuare ad utilizzare le stesse interfacce e non utilizzare
     * nuovi metodi per effettuare, ad esempio, ricerche by field
     * @param root
     * @param criteriaQuery
     * @param criteriaBuilder
     * @return
     */

    @Override
    public Predicate toPredicate(Root<Person> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        if (criteria.getOperation().equalsIgnoreCase("=")) {
            if (root.get(criteria.getKey()).getJavaType() == String.class) {
                return criteriaBuilder.like(
                        root.get(criteria.getKey()), "%" + criteria.getValue() + "%");
            } else {
                return criteriaBuilder.equal(root.get(criteria.getKey()), criteria.getValue());
            }
        }
        return null;
    }
}
