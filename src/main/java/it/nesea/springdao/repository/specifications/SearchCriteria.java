package it.nesea.springdao.repository.specifications;

import lombok.Getter;
import lombok.Setter;

/**
 * Aggiungere un oggetto per creare i filtri di ricerca può rendere
 * generico il filtering e quindi, si potrà fare uso di questo per tutte le collection
 * presenti nel sistema
 */

@Getter
@Setter
public class SearchCriteria {
    private String key;
    private String operation;
    private Object value;
}
