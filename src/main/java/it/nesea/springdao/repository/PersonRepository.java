package it.nesea.springdao.repository;

import it.nesea.springdao.model.Person;
import it.nesea.springdao.repository.customized.CustomizedPersonRepository;
import it.nesea.springdao.repository.projections.NoID;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepository extends CommonRepository<Person, Integer>, JpaSpecificationExecutor<Person>, CustomizedPersonRepository {
    Person findByName(String name);
    List<NoID> findByEmailAndLastName(String emailAddress, String lastname);
    @Query("SELECT p FROM Person p WHERE p.email LIKE %?1%")
    List<Person> findByEmailLike(String email);

    List<Person> findDistinctByTeam_Id(Integer teamId);
}
