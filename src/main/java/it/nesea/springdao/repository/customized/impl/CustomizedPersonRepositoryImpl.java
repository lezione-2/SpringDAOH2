package it.nesea.springdao.repository.customized.impl;

import it.nesea.springdao.model.Person;
import it.nesea.springdao.repository.customized.CustomizedPersonRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import java.util.List;
import java.util.Objects;

@Repository
public class CustomizedPersonRepositoryImpl implements CustomizedPersonRepository {
    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    private EntityManager entityManager;

    @Override
    public List<Person> myCustomSearchByName(String name) {
        Query query = entityManager.createQuery("SELECT e FROM Person e WHERE e.name = :name");
        query.setParameter("name", Objects.requireNonNull(name, "Name cannot be null"));
        return query.getResultList();
    }
}
