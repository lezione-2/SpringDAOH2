package it.nesea.springdao.repository.customized;

import java.util.List;

/**
 * Per una valida ragione, si può scegliere di svincolarsi dal repository ed andare ad implementare una nostra interfaccia.
 * Il procedimento è simile a quello che abbiamo visto nel DAO, l'importante sarà procurarsi l'entityManager.
 * Al di là di questo, bisognerà che il nostro repository, in questo caso @PersonRepository, la implementi.
 *
 * Spring Data sarà in grado di unire le due implementazioni in maniera a noi del tutto trasparente.
 * @param <T>
 */
public interface CustomizedPersonRepository<T> {
    List<T> myCustomSearchByName(String name);
}
