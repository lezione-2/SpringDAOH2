package it.nesea.springdao.repository.projections;

/**
 * Questa interfaccia viene usata per effettuare la proiezione.
 * Utilizzata nella Proiezione Chiusa, questa permette di ottimizzare
 * la query. Spring userà solamente i campi qui riportati.
 * Il risultato sarà un proxy che ci permetterà di ricavare i dati usando
 * i getter specificati nell'interfaccia.
 *
 * ATTTENZIONE: tali getter devono essere gli stessi presenti nell'entity
 */
public interface NoID {
    String getName();
    String getLastName();
    String getEmail();
}
