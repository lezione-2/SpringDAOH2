package it.nesea.springdao.dao.impl;

import it.nesea.springdao.dao.DaoPerson;
import it.nesea.springdao.model.Person;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.persistence.*;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;

@Component
public class PersonDAO implements DaoPerson<Person> {

    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;

    private EntityManager em;

    @PostConstruct
    public void init() {
        this.em = entityManagerFactory.createEntityManager();
    }

    @Override
    public Optional<Person> get(long id) {
        return Optional.ofNullable(em.find(Person.class, id));
    }

    @Override
    public List<Person> getAll() {
        Query query = em.createQuery("SELECT e FROM Person e");
        return query.getResultList();
    }

    @Override
    public void save(Person person) {
        executeInsideTransaction(entityManager -> entityManager.persist(person));
    }

    @Override
    public void update(Person person, String[] params) {
        person.setName(Objects.requireNonNull(params[0], "Name cannot be null"));
        person.setLastName(Objects.requireNonNull(params[1], "Email cannot be null"));
        executeInsideTransaction(entityManager -> entityManager.merge(person));
    }

    @Override
    public void delete(Person person) {
        executeInsideTransaction(entityManager -> entityManager.remove(person));
    }

    @Override
    public List<Person> findByName(String name){
        Query query = em.createQuery("SELECT e FROM Person e WHERE e.name = :name");
        query.setParameter("name", Objects.requireNonNull(name, "Name cannot be null"));
        return query.getResultList();
    }

    private void executeInsideTransaction(Consumer<EntityManager> action) {
        EntityTransaction tx = em.getTransaction();
        try {
            tx.begin();
            action.accept(em);
            tx.commit();
        }
        catch (RuntimeException e) {
            tx.rollback();
            throw e;
        }
    }
}
