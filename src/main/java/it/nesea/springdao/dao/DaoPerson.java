package it.nesea.springdao.dao;

import java.util.List;

public interface DaoPerson <T> extends Dao<T>{
    /**
     * Per poter effettuare la ricerca per nome, abbiamo avuto bisogno di creare un nuovo metodo nell'interfaccia
     * Questo comporta che, ogni qual volta volessimo aggiungere un fltro di ricerca, ci troviamo di fronte
     * alla necessità di aggiungere u nuovo metodo di interfaccia e quindi la sua implementazione.
     * Andando a lavorare sulle interfacce che possono avere molteplici implementazioni, corriamo il rischio
     * di dover rivedere un numero imprecisato di classi.
     * @param name
     * @return
     */
    List<T> findByName(String name);
}
