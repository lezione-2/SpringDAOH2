package it.nesea.springdao.service;

import it.nesea.springdao.dao.DaoPerson;
import it.nesea.springdao.model.Person;
import it.nesea.springdao.repository.PersonRepository;
import it.nesea.springdao.repository.specifications.PersonSpecification;
import it.nesea.springdao.repository.specifications.SearchCriteria;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class PersonServices {
    private PersonRepository personRepository;
    private DaoPerson<Person> personDao;

    public PersonServices(PersonRepository personRepository, DaoPerson<Person> personDao) {
        this.personRepository = personRepository;
        this.personDao = personDao;
    }

    @Transactional
    public void saveStubPersonRepo(){
        Person p = new Person();
        p.setLastName("Rossi");
        p.setName("Mario");
        personRepository.save(p);
    }


    public void saveStubPersonDAO(){
        Person p = new Person();
        p.setLastName("Verdi");
        p.setName("Giuseppe");
        personDao.save(p);
    }

    public List<Person> findByNameDAO(String name){
        return personDao.findByName(name);
    }

    public List<Person> findByNameRepo(String name){
        SearchCriteria criteria = new SearchCriteria();
        criteria.setKey("name");
        criteria.setOperation("=");
        criteria.setValue(name);
        return personRepository.findAll(new PersonSpecification(criteria));
    }

    public long countByRepo(){
        return personRepository.count();
    }

    public long countByDAO(){
        return personDao.getAll().size();
    }

    public List<Person> findTeamMembers(Integer teamId){
        return personRepository.findDistinctByTeam_Id(teamId);
    }
}
