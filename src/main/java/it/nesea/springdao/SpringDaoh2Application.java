package it.nesea.springdao;

import it.nesea.springdao.model.Person;
import it.nesea.springdao.service.PersonServices;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.List;

@SpringBootApplication
public class SpringDaoh2Application {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(SpringDaoh2Application.class, args);
        PersonServices personServices = (PersonServices)run.getBean("personServices");
        personServices.saveStubPersonRepo();
        personServices.saveStubPersonDAO();

        List<Person> personListDAO = personServices.findByNameDAO("Giuseppe");
        personListDAO.forEach(person -> System.out.println("person = " + person));
        System.out.println("++++++++++++++++");
        List<Person> personList = personServices.findByNameRepo("Luigi");
        personList.forEach(person -> System.out.println("person = " + person));
        System.out.println("++++++++++++++++");
        personList = personServices.findTeamMembers(1);
        personList.forEach(person -> System.out.println("person = " + person));
    }
}
