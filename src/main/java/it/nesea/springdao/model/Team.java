package it.nesea.springdao.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString(exclude = "personSet") //Abbiamo escluso questo campo per non creare cicli nelle bidirezionalità della relazione
public class Team {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEAM_ID_SEQ_GEN")
    @SequenceGenerator(name = "TEAM_ID_SEQ_GEN", sequenceName = "TEAM_ID_SEQ", allocationSize = 10)
    private Integer id;
    private String name;
    @OneToMany(mappedBy = "team")
    private Set<Person> personSet = new HashSet<>();

}
