package it.nesea.springdao.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PERSON_SEQ_GEN")
    @SequenceGenerator(name = "PERSON_SEQ_GEN", sequenceName = "person_id_seq", allocationSize = 10)
    private Integer id;
    private String name;
    private String lastName;
    private String email;
    @ManyToOne
    @JoinColumn(name="team_id", nullable=false)
    private Team team;
}
