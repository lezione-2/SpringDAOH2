CREATE SEQUENCE IF NOT EXISTS SPRINGDAO.PERSON_ID_SEQ START WITH 1 INCREMENT BY 10;
CREATE SEQUENCE IF NOT EXISTS SPRINGDAO.TEAM_ID_SEQ START WITH 1 INCREMENT BY 10;


CREATE TABLE IF NOT EXISTS  SPRINGDAO.TEAM(
    id serial primary key,
    name  varchar(30)
);

CREATE TABLE IF NOT EXISTS  SPRINGDAO.PERSON(
    id serial primary key,
    name varchar(30),
    last_name varchar(30),
    email varchar(50),
    team_id  integer,
    constraint fk_team foreign key (team_id) references SPRINGDAO.TEAM(id)
);


