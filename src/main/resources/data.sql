insert into  SPRINGDAO.TEAM (id, name) values (1,'springBootter');
insert into  SPRINGDAO.TEAM (id, name) values (2,'ejbRulez');

insert into  SPRINGDAO.PERSON (id, last_name, name, email, team_id) values (SPRINGDAO.PERSON_ID_SEQ.NEXTVAL,'Bianchi', 'Luigi', 'luigi.bianchi@nesea.it',1);
insert into  SPRINGDAO.PERSON (id, last_name, name, email, team_id) values (SPRINGDAO.PERSON_ID_SEQ.NEXTVAL,'Neri', 'Giorgio', 'giorgio.neri@nesea.it',1);
insert into  SPRINGDAO.PERSON (id, last_name, name, email, team_id) values (SPRINGDAO.PERSON_ID_SEQ.NEXTVAL,'Giallo', 'Michele', 'michele.giallo@nesea.it',2);
insert into  SPRINGDAO.PERSON (id, last_name, name, email, team_id) values (SPRINGDAO.PERSON_ID_SEQ.NEXTVAL,'Viola', 'Flavio', 'flavio.viola@nesea.it',2);
insert into  SPRINGDAO.PERSON (id, last_name, name, email, team_id) values (SPRINGDAO.PERSON_ID_SEQ.NEXTVAL,'Bianchi', 'Giovanni', 'giovanni.bianchi@nesea.it',2);