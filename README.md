# Spring DAO H2
### per il corso Spring for Beginner

##### Project info
Esempio dell'utilizzo del DAO pattern con spring boot. In questo esempio troverai una configurazione avanzata per il database
in memory H2.
Seguendo gli esempi potrai comprendere come eseguire il bootstrap dello schema in due diverse modalità:

- Attraverso hibernate e la mappatura delle entity con l'opzione ddl.auto abilitata
- Attraverso spring boot ed i file schema-**.xml e data-**.xml

Affiancato al dao pattern vi sarà la risposta equivalente di spring con Repository. Questa metterà in luce
la semplicità con il quale è possibile operare sulla persistenza dei dati

Verranno quindi scritte le prime query e specifications di esempio.


##### Boot dell'applicazione

Per avviare il progetto è possibile eseguire il comando

> mvn spring-boot:run

Questo avvierà l'applicazione.

Sarà possibile consultare la console del databese H2 al seguente link:

> http://localhost:8080/h2-console/

ricordandosi di inserire come jdbc url

> jdbc:h2:mem:springdao

##### Nota bene:
In questo pom viene introdotto spring web ma non viene concretamente usato nella lezione
due. La sua introduzione è legata all'utilizzo dell'embedded tomcat indispensabile per la console h2

Una via alternativa sarebbe potuta essere la configurazione del plugin maven jetty.